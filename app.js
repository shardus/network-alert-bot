#!/usr/bin/env node
/* jshint node: true, esversion: 8 */
'use strict';

const { Client, GatewayIntentBits, EmbedBuilder, Attachment }   = require('discord.js');
const client = new Client({ intents: [ GatewayIntentBits.Guilds, GatewayIntentBits.GuildMessages, GatewayIntentBits.MessageContent ] });
const filer = require('fs');
const config = require('./config.json');
const dotConfig = require('dotenv').config();

let controller = null;
let checkInterval = config.checkInterval;
let archiverNodeListURI = "/full-nodelist"
let nodeInfoURI = "/nodeinfo"
let logDir = "./logs/"
let stateDir = "./state/"


client.on('ready', () => {
    console.log(`Bot has started, with ${client.users.cache.size} users, in ${client.channels.cache.size} channels of ${client.guilds.cache.size} guilds.`);
    client.command
    client.channels.cache.get(process.env.CHANNEL).send('Bot started.');
    controller = setInterval(monitorEnvs, checkInterval);
  });

client.on('error', console.error);
  
client.login(process.env.TOKEN);

client.on("messageCreate", async message => {
    if(message.author.bot) return;
    //let chTest1 = message.guild.channels.find("name", config.requests);
    const memberRequestingAction = message.member.user.username;
    const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
    let command = args.shift().toLowerCase();
    if(command === "up") {
      const m = await message.channel.send({ content: `Am I??` });
      m.edit(`I'm up! Latency is ${m.createdTimestamp - message.createdTimestamp}ms. I've been up ${Math.round(client.uptime / 1000 )} seconds`);
    }
    if(command === "check") {
        let m = await message.channel.send({ content: `Checking.` });
        monitorEnvs();
    }
    if(command === "stop") {
        let m = await message.channel.send({ content: `Stopping.` });
        clearInterval(controller);
        m.edit({ content: 'Stopped.' });
    }
    if(command === "start") {
        let m = await message.channel.send({ content: `Starting.` });
        controller = setInterval(monitorEnvs, checkInterval);      
        m.edit({ content: 'Started.' });
    }
    if(command === "interval") {
        checkInterval = args[0];
        let m = await message.channel.send({ content: `Changing check interval from ${config.checkInterval} to ${checkInterval}` });
        clearInterval(controller);
        controller = setInterval(monitorEnvs, checkInterval);      
        m.edit({ content: `Check interval changed to ${checkInterval}` });
    }
    if(command === "help") {
        let m = await message.channel.send({ content: `Commands are: up, check, stop, start, interval, help` });
    }
});

async function monitorEnvs() {
    for(let envName in config.work) {
        if(config.work[envName].deployed === true) {
            let logAndStateFiles = stateAndLogFileHandling(envName);
            let envState = loadEnvState(envName);
            let archiverURL = "http://" + config.work[envName].archiver + archiverNodeListURI;
            getData(archiverURL).then((data) => {
                for(let node in data) {
                    if(node === "nodeList") {
                        //console.log(`Found the following nodes in ${envName}:`);
                        data.nodeList.forEach((nod) => {
                            //console.log(nod.ip, nod.port);
                            let nodeURL = "http://" + nod.ip + ":" + nod.port + nodeInfoURI;
                            getData(nodeURL).then((data) => {
                                if (nodeObjectTest(data)) {
                                    if (data.nodeInfo.status === "active") {
                                        console.log(envName, nod.ip, nod.port, " Node is up");
                                        //client.channels.cache.get(process.env.CHANNEL).send(`${envName} ${nod.ip} ${nod.port} Node is active`);
                                        writeLog(logAndStateFiles.upState, nod.ip + " " + nod.port);
                                        writeLog(logAndStateFiles.log, nod.ip + " " + nod.port + " " + "active");
                                        updateState(envName, nod.ip + " " + nod.port, "active");
                                    } else if (data.nodeInfo.status === "syncing") {
                                        console.log(envName, nod.ip, nod.port, " Node is syncing");
                                        //client.channels.cache.get(process.env.CHANNEL).send(`${envName} ${nod.ip} ${nod.port} Node is active`);
                                        writeLog(logAndStateFiles.syncState, nod.ip + " " + nod.port);
                                        writeLog(logAndStateFiles.log, nod.ip + " " + nod.port + " " + "syncing");
                                        updateState(envName, nod.ip + " " + nod.port, "syncing");
                                    } else {
                                        console.log(envName, nod.ip, nod.port, " Node is null");
                                        //client.channels.cache.get(process.env.CHANNEL).send(`${envName} ${nod.ip} ${nod.port} Node is not active`);
                                        sendDiscordMessage(`${envName} ${nod.ip} ${nod.port} Node is possibily joining: http://${config.work[envName].monitor}/`);
                                        writeLog(logAndStateFiles.downState, nod.ip + " " + nod.port);
                                        writeLog(logAndStateFiles.log, nod.ip + " " + nod.port + " " + "down");
                                        updateState(envName, nod.ip + " " + nod.port, "down");
                                    }
                                } else {
                                    console.log(envName, nod.ip, nod.port, " Node assumed up by GET test.");
                                    //client.channels.cache.get(process.env.CHANNEL).send(`${envName} ${nod.ip} ${nod.port} Node is active`);
                                    writeLog(logAndStateFiles.upState, nod.ip + " " + nod.port);
                                    writeLog(logAndStateFiles.log, nod.ip + " " + nod.port + " " + "active");
                                    updateState(envName, nod.ip + " " + nod.port, "active");
                                }
                                //console.log(data);
                            }).catch((err)  => {
                                console.log(envName, nod.ip, nod.port, " Node is not reachable. (down/crashed)");
                                sendDiscordMessage(`${envName} ${nod.ip} ${nod.port} Node is down: http://${config.work[envName].monitor}/`);
                                writeLog(logAndStateFiles.downState, nod.ip + " " + nod.port);
                                writeLog(logAndStateFiles.log, nod.ip + " " + nod.port + " " + "down");
                                updateState(envName, nod.ip + " " + nod.port, "down");
                            });
                        });
                    }
                }
            }).catch((err)  => {
                console.log(`Cannot access archiver specified for ${envName}. Check the config.json entry for work.${envName}.archiver`);
            });
        }
    }
}

async function getData(url) {
    const response = await fetch(url);
    if (!response.ok) {
        const message = `${response.status}`;
        return message;
    }
    const nodeData = await response.json();
    return nodeData;

} 

function writeLog(log, line) {
        filer.readFile(log, function (err, data) {
            if (err) throw err;
            if(data.includes(line)){
                //console.log("Already in log");
            } else {
                filer.appendFile(log, line
                    + "\n", function (err) {
                    if (err) throw err;
                    //console.log('Saved!');
                   });
                }
        });
}

function sendDiscordMessage(msg) {
    const channel = client.channels.cache.get(process.env.CHANNEL);
    let sendIt = false;
    let alreadySent = false;
    channel.messages.fetch({ limit: 100 }).then(messages => {
        //console.log(`Received ${messages.size} messages`);
        //Iterate through the messages here with the variable "messages".
        messages.forEach(message => {
            if(message.content.includes(msg)) {
                sendIt = false;
                alreadySent = true;
                //console.log("Already sent");
                return;
            } else {
                sendIt = true;
                //console.log("Sending message");
                return;
            }
        });
    }).then(() => {
        if(sendIt === true && !alreadySent) {
            channel.send(msg);
        }
    });
}

function stateAndLogFileHandling(envi) {
    let files = {
        log: logDir + envi + ".log",
        upState: stateDir + envi + ".up",
        syncState: stateDir + envi + ".sync",
        downState: stateDir + envi + ".down"
    }
    for (let file in files) {
        if (!filer.existsSync(files[file])) {
            filer.writeFileSync(files[file], "");
        }
    };
    return files;
}

function loadEnvState(envi) {
    let envState = {
        up: [],
        sync: [],
        down: []
    }
    let files = stateAndLogFileHandling(envi);
    for (let file in files) {
        if (filer.existsSync(files[file])) {
            let data = filer.readFileSync(files[file], 'utf8');
            let lines = data.split(/\r?\n/);
            lines.forEach((line) => {
                if (line !== "") {
                    if (file === "upState") {
                        envState.up.push(line);
                    } else if (file === "syncState") {
                        envState.sync.push(line);
                    } else if (file === "downState") {
                        envState.down.push(line);
                    }
                }
            });
        }
    }
    return envState;
}

// a function that takes in env, node and state and removes the node from one state file and adds it to the current state file
function updateState(env, node, state) {
    let files = stateAndLogFileHandling(env);
    let envState = loadEnvState(env);
    if (state === "active") {
        if (envState.up.includes(node)) {
            //console.log("Node is already in up state");
        } else {
            if (envState.sync.includes(node)) {
                removeIt(files.syncState, node);
                removeIt(files.log, node + " syncing");
            } else if (envState.down.includes(node)) {
                removeIt(files.downState, node);
                removeIt(files.log, node + " down");
            }
        }
    }
    if (state === "syncing") {
        if (envState.sync.includes(node)) {
            //console.log("Node is already in sync state");
        } else {
            if (envState.up.includes(node)) {
                removeIt(files.upState, node);
                removeIt(files.log, node + " active");
            } else if (envState.down.includes(node)) {
                removeIt(files.downState, node);
                removeIt(files.log, node + " down");
            }
        }
    }
    if (state === "down") {
        if (envState.down.includes(node)) {
            //console.log("Node is already in down state");
        } else {
            if (envState.up.includes(node)) {
                removeIt(files.upState, node);
                removeIt(files.log, node + " active");
            } else if (envState.sync.includes(node)) {
                removeIt(files.syncState, node);
                removeIt(files.log, node + " syncing");
            }
        }
    }
}

function removeIt(file, node, state) {
    let work = "";
    if (state !== undefined) {
        work = node + " " + state;    
    } else {
        work = node;
    }
    filer.readFile(file, 'utf8', function (err, data) {
    if (err) {
        return console.log(err);
    }
    let lines = data.split('\n')
    let filtered_lines = lines.filter(line => line !== work);
    //let result = data.replace(node, "");
    let result = filtered_lines.join('\n');
    filer.writeFile(file, result, 'utf8', function (err) {
        if (err) return console.log(err);
    });
});
}

function nodeObjectTest(obj) {
    if (obj === null) {
        return false;
    }
    if (typeof obj === 'string') {
        return false;
    }
    if (typeof obj === 'object') {
        return true;
    }
}
