# Alert Bot

This is a bot for monitoring deployed environments

Edit the config.json to specify the environments to monitor.

Add the discord bot token and chanel id for it to post into channel.

The polling interval is configurable using the checkInterval section in the config.json file.

The bot monitors `/full-nodlist` on the archiver, then pulls status data from node's `/nodeinfo` on that list.

## Getting started

This uses a few new modules available in node 19.


```nvm use 19```


`npm install`

Edit the config.json with the environments, discord token and channel ID for it to post in.

`npm run start` OR `./app.js` (This can also be managed by pm2)

The bot also writes a log file in its running directory. Only new/changed entries get added.


## Deploy:

1. Invite the Bot to the Discord Server by creating a link on the Discord Developer's OAuth2 > URL Generator's page.
- Select `bot` in the first box.
- Select the following in the second box:
- Send Message
- Manage Messages
- Add Reactions
- Use Slash Commands
- Press the `Copy` button into your Browser and select the server to invite your bot to.

2. Create a channel for the Bot to alert in.
- Add the bot's role to this channel.
- Select the same permission (above) for that role in in the Channel Settings.

3. On the server you have deployed the bot:
- Copy the .env-sample file to .env and edit the values for the bot.
- `TOKEN` - the Discord Bot token from the Discord Developer's Bot page.
- `CHANNEL` - the channel ID obtain by right clicking and selecting `Copy ID`
---

The Bot monitors each environment, every 60 seconds by default.

Note: For 1.x and 2.x, it tests for with a simple GET method; if 404 is returned, the node is assumed to be up.

When BetaNet goes live, it will be able to parse the /nodeInfo for status.

This is in the liberty-alphanets channel. Please keep conversations off this channel.

The bot listens for these commands: 
- `/up` - It will respond with its up time (or not if it is down).
- `/stop` - to stop the bot.
- `/check` - to launch a one time check.
- `/start` - to start the bot's monitoring loop.
- `/interval` - to change the monitor interval (for now in milliseconds, default: 60000)


---
### TODO:
1. rework the writeLog function to overwrite nodes' entries when their status changes
2. rework the sendDiscordMessage to clear/delete the node down message.
3. ~~Find out why state/status is lost when bot is restarted.~~
4. ~~Get this working with environemnts not in debug mode.~~
5. Add `/pfw` command back in to output results from the `pull-from-aws` application
6. Change from `/nodeinfo` to `/nodeInfo` on next release push.